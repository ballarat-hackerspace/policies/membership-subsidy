Policy active. Ratified by the committee on December 10th, 2022 (specifically the version as of this commit).


Membership Subsidy Policy
--
The Ballarat Hackerspace has members from a wide variety of backgrounds and understands that hardships can affect anyone. 
The Hackerspace is a community-run organisation, and we welcome assistance with running and improving the space and the community. 
To assist with these goals, this Membership Subsidy Policy allows members to help with running the Hackerspace, in return for a subsidy on their membership fees.

Membership Subsidy is only available for Full-Time, or above, levels (e.g Seeders).
The Casual membership allows access to the Hackerspace during our nominated opening times only.
Further, a Casual Member cannot open the space themselves.
We price our casual membership rate to be very cheap for the value it provides.
If there was an opportunity to reduce the price, it would already be reduced.

For an eligible member, they can assist the space by being the nominated "Duty Member" and opening the space for an allotted time during our nominated opening hours for casual visits. 

In return for taking on a "shift", the Duty Member will receive $5 off their membership for the following month.
Multiple shifts can be taken per month by the same person, at $5 per shift. However the total paid cannot be reduced further than the current casual rate for any given month.

Currently available shifts are subject to the decision of the committee and may change.

Note that shifts vary in length.
The aim is to have opening hours broken into no more than two shifts, reducing scheduling complexity. 
Additional shifts are not currently available - to add casual opening hours, we need to commit long-term (6+ months) to the new hours. 
If a Member would like to open the space at a new time, please speak to the committee about this and we can discuss the suggestion.

## Responsibilities
The Duty Member responsible for opening the space is likely to be regularly interrupted during their shift. 
These interruptions will impact productivity for that Member, and this needs to be accepted before undertaking a shift.
During opening times, the Duty Member needs to:
* Be on time for opening the space
* Close the space at the allotted time. Shifts can end up to half an hour early if there is nobody else (or only Full-Time members with 24/7 access) in the space.
* Ensure all members are adhering to relevant safety and inclusiveness policies.
* Greet new members and visitors, and show them around the Hackerspace.
* Answer questions about the Hackerspace, for instance - how to use the equipment, or where tools are kept.
* Clean any spills or messes, if the person who caused it cannot be identified.
* Empty bins if they are full.
* Other actions (that can change from time to time), as outlined in our procedures: https://gitlab.com/ballarat-hackerspace/services/workshop/wikis/home

## Eligibility criteria
Not all members are eligible to undertake a shift. A member must meet the following requirements:
* Member must be a Full-Time member for at least three months
* Member must not have had a complaint confirmed against them through any other policy (such as the Code of Conduct at https://ballarathackerspace.org.au/code-of-conduct) for the last 12 months
* Member must be an active member of the Hackerspace
* Member must adhere to the opening and closing policy and procedures in particular
* Member must be aware of, and adhere to, all the policies of the Ballarat Hackerspace.

For Wednesday shifts, the Member must be female or be the guardian of a female child/teenager who is actively using the space during that shift.

